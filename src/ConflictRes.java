/**
 * @author WOLFEBD
 * For practicing merge conflict resolution with Git and EGit
 */
public class ConflictRes {

	public static String yell(String s){
		// A change that shouldn't cause a conflict
		return "** " + s.toUpperCase() + " **";
	}
	
	public static void main(String[] args) {
		// here is another change
		String message = "merge conflicts are no fun, but they are better than emailing code back and forth";
		System.out.println( yell(message) );
	}

}
